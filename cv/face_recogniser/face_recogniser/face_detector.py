import dlib
import numpy as np


def face_detection(pimg):
    """
    face_detection function receives an PIL format image and
    returns a list of detected faces
    :param pimg: PIL format image
    :return: bb is the bounding box around the detected face

    #>>> pimg = Image.open('test/img_test.jpg').convert('RGB')
    #>>> face_detection(pimg)

    """
    detector = dlib.get_frontal_face_detector()

    img = np.array(pimg);
    # The 1 in the second argument indicates that we should upsample the image
    # 1 time.  This will make everything bigger and allow us to detect more
    # faces.

    dets = detector(img, 1)


    print(type(dets))

    print("Number of faces detected: {}".format(len(dets)))

    bb = []

    for i, d in enumerate(dets):

        bb.append([d.left(), d.top(), d.right(), d.bottom()])
        print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
            i, d.left(), d.top(), d.right(), d.bottom()))

    return bb



def face_detection_cnn(pimg):
    """
    face_detection_cnn function receives an PIL format image and
    returns a list of detected faces
    :param pimg: PIL format image
    :return: bb is the bounding box around the detected face

    #>>> pimg = Image.open('test/img_test.jpg').convert('RGB')
    #>>> face_detection(pimg)

    """
    face_detector_model = '/Users/mdiazmel/data/face_det_models/mmod_human_face_detector.dat'

    cnn_face_detector = dlib.cnn_face_detection_model_v1(face_detector_model)

    img = np.array(pimg);
    # The 1 in the second argument indicates that we should upsample the image
    # 1 time.  This will make everything bigger and allow us to detect more
    # faces.
    dets = cnn_face_detector(img, 1)

    print(type(dets))

    print("Number of faces detected: {}".format(len(dets)))

    bb = []

    for i, d in enumerate(dets):

        bb.append([d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()])
        print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
            i, d.rect.left(), d.rect.top(), d.rect.right(), d.rect.bottom()))

    return bb
