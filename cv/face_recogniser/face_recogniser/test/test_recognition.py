import face_recogniser
import unittest
from PIL import Image
import numpy as np
import time

class TestReader(unittest.TestCase):
    def test_face_recognition_location_cnn(self):
        pimg = Image.open('img_test.jpg').convert('RGB')
        start_time = time.time()
        img = np.array(pimg);
        bounding_boxes = face_recogniser.face_locations(img, 1, "cnn")
        print("--- %s seconds ---" % (time.time() - start_time))
        self.assertEqual(bounding_boxes, [(282, 701, 380, 603)])

    def test_face_recognition_location_hog(self):
        pimg = Image.open('img_test.jpg').convert('RGB')
        start_time = time.time()
        img = np.array(pimg);
        bounding_boxes = face_recogniser.face_locations(img, 1, "hog")
        print("--- %s seconds ---" % (time.time() - start_time))
        self.assertEqual(bounding_boxes, [(282, 712, 411, 583)])

if __name__  == '__main__':
    unittest.main()