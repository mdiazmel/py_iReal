# -*- encoding: utf-8 -*-
# pip install flask

import flask
from flask import request
from PIL import Image
import face_recogniser.face_recogniser as fr
import io
import numpy as np
import time
import re


app = flask.Flask(__name__) # Crée un server web
                            # qui va gérer les URL qui lui sont envoyées

# Load faces models to recognise (this function can be in another file)
def load_model_faces(filename):
    with open(filename) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    l = []
    for i in range(len(content)):
        if not(i % 2):
            name = content[i];
            val_str = content[i+1]
            val = [float(i) for i in val_str.split(',')]
            l.append((name, val))
    faces_model = dict(l)

    return faces_model



# Configure the flask server
app.config['JSON_SORT_KEYS'] = False

# Load the features to compare and recognise faces
filename_faces_to_recognise = "/home/mdiazmel/data/photosDemoDec17/faceEncodings.txt"
faces_to_compare = load_model_faces(filename_faces_to_recognise)
faces_to_compare_np = np.array(list(faces_to_compare.values()))
print(faces_to_compare.keys())

# enregistrer une route qui renvoit du texte brut
# http://127.0.0.1:9099/hello
@app.route("/cv")
def hello():
    return "Computer vision server ...!"



# renvoyer du texte généré à partir d'une template
# http://127.0.0.1:9099/hello.html
@app.route('/bonjour.html')
def bonjour_html():
    mots = ["bonjour", "à", "toi,", "visiteur."]
    return flask.render_template('hello.html'
                                 , titre="Bienvenue !"
                                 , mots=mots)


# renvoyer du json, utiliser un route variable
# http://127.0.0.1:9099/api/product/A
PRODUCTS = {
   "A": {'title': 'Mouse', 'price': 50.},
   "B": {'title': 'Keyboard', 'price': 45.}
}         

# respecte les conventions d'une api REST
@app.route("/api/product/<id>")
def product(id=None):
    # Si on saisi http://localhost:8081/api/product/A => id='A'
    product_info = PRODUCTS[id]
    return flask.jsonify(identifiant=id
                         , titre= product_info['title']
                         , prix= product_info['price'])


# créer une route spécifique à une méthode
# ajouter des données avec put
# renvoyer un code de succès autre que 200
# POST http://127.0.0.1:9099/api/product/C  (utiliser addon firefox REST easy)
# puis pour consulter le résultat: http://127.0.0.1:5000/api/product/C

@app.route("/api/product/<id>",
           methods=["PUT", "POST"])
def edit_product(id=None):

    data = "Received data:\n"
    # data += request.form['test']
    data += "\n" + request.data.decode('utf8')
    data += "param1:" + request.form['param1']
    data += ", "
    data += "param2:" + request.form['param2']

    return data


@app.route("/api/cv/face_detection/", methods=['POST'])
def api_face_detection():
    try:
        for f in request.files:
            img = request.files[f]
            print(f, type(img))
            print(dir(img))
            print(type(img.stream))
            print(dir(img.stream))

            pimg = Image.open(img.stream)
            npimg = np.array(pimg)
            bounding_boxes = fr.face_locations(npimg)
            #pimg.show()

            faces_list = list()
            for bb in bounding_boxes:
                print(bb)
                bb_dic = {"left": bb[3],
                          "top": bb[0],
                          "width": bb[1] - bb[3],
                          "height": bb[2] - bb[0]}
                print(bb_dic)
                faceId = "unknown"
                faces_list.append({"faceId": faceId,
                                   "faceRectangle": bb_dic})

            response_list = [("faces", faces_list),
                             ("metadata",
                              {"width": pimg.width,
                               "height": pimg.height,
                               "format": img.content_type})]

            response_dict = dict(response_list)

            print(response_dict)
            return flask.jsonify(response_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"


@app.route("/api/cv/face_detection_cnn/", methods=['POST'])
def api_face_detection_cnn():
    try:
        for f in request.files:
            img = request.files[f]
            # print(f, type(img))
            # print(dir(img))
            # print(type(img.stream))
            # print(dir(img.stream))

            pimg = Image.open(img.stream)
            npimg = np.array(pimg)
            bounding_boxes = fr.face_locations(npimg, 1, 'cnn')
            #pimg.show()

            faces_list = list()
            for bb in bounding_boxes:
                # print(bb)
                bb_dic = {"left": bb[3],
                          "top": bb[0],
                          "width": bb[1]-bb[3],
                          "height": bb[2]-bb[0]}
                # print(bb_dic)
                faceId = "unknown"
                faces_list.append({"faceId": faceId,
                                  "faceRectangle": bb_dic})

            response_list = [("faces", faces_list),
                             ("metadata",
                              {"width": pimg.width,
                               "height": pimg.height,
                               "format": img.content_type})]

            response_dict = dict(response_list)

            print(faces_list)
            print('/n')
            return flask.jsonify(response_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"


@app.route("/api/cv/face_recognition/", methods=['POST'])
def api_face_recognition():
    try:
        for f in request.files:
            img = request.files[f]

            start = time.time()
            pimg = Image.open(img.stream)
            npimg = np.array(pimg)
            bounding_boxes = fr.face_locations(npimg, 1, 'cnn')
            face_encodings = fr.face_encodings(npimg, bounding_boxes)
            #print(len(face_encodings))
            face_names = []
            #print(faces_to_compare.keys())

            for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                #print(type(faces_to_compare_np))
                #print(faces_to_compare_np.shape)
                #print(type(face_encoding))
                #print(face_encoding.shape)
                #match = fr.compare_faces(faces_to_compare_np, face_encoding)
                distances = np.linalg.norm(faces_to_compare_np - face_encoding, axis=1)
                tolerance = 0.6
                match = list(distances <= tolerance)
                #print(match)
                name = "Unknown__"
                true_index = [i for i, x in enumerate(match) if x]

                if any(match):
                    name = list(faces_to_compare.keys())[true_index[0]]

                face_names.append(name)


            faces_list = list()
            #for bb in bounding_boxes:
            for idx, bb in enumerate(bounding_boxes):
                #print(bb)
                bb_dic = {"left": bb[3],
                          "top": bb[0],
                          "width": bb[1]-bb[3],
                          "height": bb[2]-bb[0]}
                #print(bb_dic)
                #faceId1 = face_names[0][:-2]
                faceId1 = face_names[idx][:-2]
                faceId2 = re.sub(r"(\w)([A-Z])", r"\1 \2", faceId1)
                faceId = faceId2.title()

                faces_list.append({"faceId": faceId,
                                  "faceRectangle": bb_dic})

            response_list = [("faces", faces_list),
                             ("metadata",
                              {"width": pimg.width,
                               "height": pimg.height,
                               "format": img.content_type})]

            response_dict = dict(response_list)

            #print(response_dict)
            print(faces_list)
            end = time.time()
            print("Processing time: {} ".format(end - start))
            return flask.jsonify(response_dict)

    except Exception as err:
        print("ko:", err)

    return "ok"


@app.route("/api/product/add", methods=["POST"])  # respecte les conventions d'une api REST
def add_product():
    id = request.form['id']
    print('PRODUITS en base:', PRODUCTS)

    print("Reçu:", request.form)
    print("ID:*%s*" % id)
    if id not in PRODUCTS:
        print("Pas trouvé")
        PRODUCTS[id] = {
           'title': request.form['title']
           , 'price': float(request.form['price'])
        }
        print("Updated")
        print(PRODUCTS[id])
        return "", 204  # 204 est le code d'erreur HTTP pour mise à jour
    else:
        return ("Aborting")
        # abort(400)

# rediriger vers une autre url
from flask import redirect, url_for
@app.route('/')
def index():
    return redirect(url_for('hello_html'))


# envoyer un code d'erreur
from flask import abort
@app.route('/broken')
def broken_url():
    abort(401)


# exercices
# renvoyer une erreur 404 si on essaye d'afficher un produit qui n'existe pas
# ajouter une route permettant de créer un produit existant (méthode POST)
# renvoyer une 400 si on essaye de créer un produit avec un id qui existe déjà
# faire un affichage html d'une fiche produit (s'inspirer de la template hello.html)


if __name__ == "__main__":
    # app.config.update(MAX_CONTENT_LENGTH=100*10**6)
    app.run(port=9099
            ,host='0.0.0.0')
    

